package edu.bergstrom.cxflab.service;

import edu.bergstrom.cxflab.domain.Thing;
import org.apache.geronimo.mail.util.StringBufferOutputStream;

import javax.jws.WebService;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

@WebService
public class SomeWebServiceImpl implements SomeWebService {

    public Thing getSomeThing(Thing anotherThing) throws IOException {
        Thing newThing = new Thing();
        newThing.setNumber(anotherThing.getNumber() * anotherThing.getNumber());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        BufferedOutputStream bos = new BufferedOutputStream(gzipOutputStream);
        bos.write(anotherThing.getText().getBytes());
        bos.close();
        gzipOutputStream.close();
        byteArrayOutputStream.close();
        newThing.setText(DatatypeConverter.printBase64Binary(byteArrayOutputStream.toByteArray()));
        return newThing;
    }
}
