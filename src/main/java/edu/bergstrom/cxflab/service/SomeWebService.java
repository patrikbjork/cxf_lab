package edu.bergstrom.cxflab.service;

import edu.bergstrom.cxflab.domain.Thing;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;

@WebService
public interface SomeWebService {

    @WebMethod
    public Thing getSomeThing(Thing anotherThing) throws IOException;
}
