package edu.bergstrom.cxflab.client;

import edu.bergstrom.cxflab.domain.Thing;
import edu.bergstrom.cxflab.security.ClientCallbackHandler;
import edu.bergstrom.cxflab.service.SomeWebService;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientFactoryBean;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.handler.WSHandlerConstants;

import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.logging.Logger;

public class SomeClient {

    static final Logger LOGGER = Logger.getLogger(SomeClient.class.getName());

    public static void main(String[] args) throws Exception {
        SomeWebService someWebService = new SomeClient().createSomeWebService();

        Thing firstThing = new Thing();
        firstThing.setNumber(4);
        firstThing.setText("Some awesome textSome awesome textSome awesome textSome awesome textSome awesome text");

        Thing secondThing = new Thing();
        secondThing.setNumber(6);
        secondThing.setText("Some awesome text");

        Thing thirdThing = new Thing();
        thirdThing.setNumber(8);
        thirdThing.setText("Some awesome text that shouldn't be as easy to compress to a small string since it is " +
                "not consisted of one small fragment that is repeated.");

        Thing firstSomeThing = someWebService.getSomeThing(firstThing);
        Thing secondSomeThing = someWebService.getSomeThing(secondThing);
        Thing thirdSomeThing = someWebService.getSomeThing(thirdThing);

        LOGGER.info(firstSomeThing.toString());
        LOGGER.info(secondSomeThing.toString());
        LOGGER.info(thirdSomeThing.toString());
    }

    public SomeWebService createSomeWebService() throws IOException, NoSuchAlgorithmException, KeyStoreException,
            CertificateException {

        ClientProxyFactoryBean clientProxyFactoryBean = new ClientProxyFactoryBean();
        ClientFactoryBean clientFactoryBean = clientProxyFactoryBean.getClientFactoryBean();
        clientFactoryBean.setAddress("https://localhost:8443/SomeWebService");
        clientFactoryBean.setServiceClass(SomeWebService.class);

        PrintWriter printWriter = new PrintWriter(System.out, true);

        LoggingOutInterceptor loggingOutInterceptor = new LoggingOutInterceptor(printWriter);
        loggingOutInterceptor.setPrettyLogging(true);

        LoggingInInterceptor loggingInInterceptor = new LoggingInInterceptor(printWriter);
        loggingInInterceptor.setPrettyLogging(true);

        clientFactoryBean.getOutInterceptors().add(loggingOutInterceptor);

        clientFactoryBean.getInInterceptors().add(loggingInInterceptor);

        //WSS4JOutInterceptor
        HashMap<String, Object> outProps = new HashMap<>();
        outProps.put(WSHandlerConstants.ACTION, "Signature Encrypt");
        outProps.put(WSHandlerConstants.USER, "mykey");
        outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, ClientCallbackHandler.class.getName());
        outProps.put(WSHandlerConstants.SIG_PROP_FILE, "client_key.properties");
        outProps.put(WSHandlerConstants.ENC_PROP_FILE, "client_trust.properties");
        WSS4JOutInterceptor wss4JOutInterceptor = new WSS4JOutInterceptor(outProps);

        //WSS4JInInterceptor
        HashMap<String, Object> inProps = new HashMap<>();
        inProps.put(WSHandlerConstants.ACTION, "Encrypt");
        inProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, ClientCallbackHandler.class.getName());
        inProps.put(WSHandlerConstants.DEC_PROP_FILE, "client_key.properties");
        WSS4JInInterceptor wss4JInInterceptor = new WSS4JInInterceptor(inProps);

        clientFactoryBean.getOutInterceptors().add(wss4JOutInterceptor);
        clientFactoryBean.getInInterceptors().add(wss4JInInterceptor);

        SomeWebService someWebService = clientProxyFactoryBean.create(SomeWebService.class);
        Client clientProxy = ClientProxy.getClient(someWebService);
        HTTPConduit httpConduit = (HTTPConduit) clientProxy.getConduit();
        httpConduit.setTlsClientParameters(setupTlsClientParameters());

        return someWebService;
    }

    private TLSClientParameters setupTlsClientParameters() throws KeyStoreException, NoSuchAlgorithmException,
            IOException, CertificateException {
        InputStream resourceAsStream = getClass().getResourceAsStream("/client/clienttruststore.jks");

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(resourceAsStream, "changeit".toCharArray());

        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
        trustManagerFactory.init(keyStore);

        TLSClientParameters tlsClientParameters = new TLSClientParameters();
        tlsClientParameters.setTrustManagers(trustManagerFactory.getTrustManagers());
        tlsClientParameters.setDisableCNCheck(true);

        return tlsClientParameters;
    }
}
