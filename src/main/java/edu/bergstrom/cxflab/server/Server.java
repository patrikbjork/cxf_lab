package edu.bergstrom.cxflab.server;

import edu.bergstrom.cxflab.security.ClientCallbackHandler;
import edu.bergstrom.cxflab.service.SomeWebService;
import edu.bergstrom.cxflab.service.SomeWebServiceImpl;
import org.apache.cxf.configuration.jsse.TLSServerParameters;
import org.apache.cxf.configuration.security.ClientAuthentication;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.transport.http_jetty.JettyHTTPServerEngineFactory;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.handler.WSHandlerConstants;

import javax.net.ssl.KeyManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.HashMap;

public class Server {

    private org.apache.cxf.endpoint.Server server;

    public static void main(String[] args) throws IOException, GeneralSecurityException {
        new Server().startServer();
    }

    public void startServer() throws IOException, GeneralSecurityException {
        SomeWebServiceImpl someWebService = new SomeWebServiceImpl();

        ServerFactoryBean serverFactoryBean = new ServerFactoryBean();
        serverFactoryBean.setServiceClass(SomeWebService.class);
        serverFactoryBean.setAddress("https://localhost:8443/SomeWebService");
        serverFactoryBean.setServiceBean(someWebService);

        setupServerEngineFactory();

        //WSS4JInInterceptor
        HashMap<String, Object> inProps = new HashMap<>();
        inProps.put(WSHandlerConstants.ACTION, "Signature Encrypt");
        inProps.put(WSHandlerConstants.SIG_PROP_FILE, "server_trust.properties");
        inProps.put(WSHandlerConstants.DEC_PROP_FILE, "server_key.properties");
        inProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, ClientCallbackHandler.class.getName());
        WSS4JInInterceptor wss4JInInterceptor = new WSS4JInInterceptor(inProps);
        serverFactoryBean.getInInterceptors().add(wss4JInInterceptor);

        //WSS4JOutInterceptor
        HashMap<String, Object> outProps = new HashMap<>();
        outProps.put(WSHandlerConstants.ACTION, "Encrypt");
        outProps.put(WSHandlerConstants.USER, "rsaclientcert");
        outProps.put(WSHandlerConstants.ENC_PROP_FILE, "server_trust.properties");
        outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, ClientCallbackHandler.class.getName());
        WSS4JOutInterceptor wss4JOutInterceptor = new WSS4JOutInterceptor(outProps);
        serverFactoryBean.getOutInterceptors().add(wss4JOutInterceptor);

        server = serverFactoryBean.create();
    }

    private void setupServerEngineFactory() throws IOException, GeneralSecurityException {
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
        KeyStore keyStore = KeyStore.getInstance("JKS");
        InputStream resourceAsStream = getClass().getResourceAsStream("/server/serverkeystore.jks");
        keyStore.load(resourceAsStream, "changeit".toCharArray());
        keyManagerFactory.init(keyStore, "changeit".toCharArray());

        JettyHTTPServerEngineFactory engineFactory = new JettyHTTPServerEngineFactory();
        TLSServerParameters tlsParams = new TLSServerParameters();

        ClientAuthentication clientAuth = new ClientAuthentication();
        clientAuth.setRequired(false);
        clientAuth.setWant(false);
        tlsParams.setClientAuthentication(clientAuth);
        tlsParams.setKeyManagers(keyManagerFactory.getKeyManagers());
        engineFactory.setTLSServerParametersForPort(8443, tlsParams);
    }

    public void stopServer() {
        if (server != null) server.stop();
    }
}
