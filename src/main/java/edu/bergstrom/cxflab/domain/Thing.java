package edu.bergstrom.cxflab.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Thing {

    private String text;
    private Integer number;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Thing{" +
                "text='" + text + '\'' +
                ", number=" + number +
                '}';
    }
}
