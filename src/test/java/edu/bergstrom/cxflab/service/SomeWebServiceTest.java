package edu.bergstrom.cxflab.service;

import edu.bergstrom.cxflab.server.Server;
import edu.bergstrom.cxflab.client.SomeClient;
import edu.bergstrom.cxflab.domain.Thing;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SomeWebServiceTest {

    private Server server;

    @Before
    public void setup() throws IOException, GeneralSecurityException {
        server = new Server();
        server.startServer();
    }

    @After
    public void tearDown() {
        server.stopServer();
    }

    @Test
    public void testGetSomeThing() throws Exception {
        SomeWebService someWebService = new SomeClient().createSomeWebService();

        Thing aThing = new Thing();
        aThing.setNumber(876);
        aThing.setText("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        Thing someThing = someWebService.getSomeThing(aThing);

        Integer i = aThing.getNumber() * aThing.getNumber();
        assertEquals(i, someThing.getNumber());
        assertTrue(aThing.getText().getBytes().length > someThing.getText().getBytes().length);
    }
}
